module.exports.tabletop = {
    scoreJ1: 0,
    scoreJ2: 0,
    toursRestants: 0,
    winner: 0,
    troupes: [],
};

module.exports.resetTabletop = (tabletop) => {
    console.log('reset')
    tabletop.scoreJ1 = 0;
    tabletop.scoreJ2 = 0;
    tabletop.toursRestants = 0;
    tabletop.winner = 0;
    tabletop.troupes = [];
};