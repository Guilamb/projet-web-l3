const ServerWrapper = require('../utils/telnet-client');
const tabletop = require('../models/tabletop');

exports.start = async (req, res) => {
    const aiFile1 = req.body.aiFile1;
    const aiFile2 = req.body.aiFile2;

    if(aiFile1 && aiFile2) {
        //execute the py files if they exist
        
    }
    else {
        const { spawn } = require('child_process');
        const server = ServerWrapper.getServerWrapper();
        if(aiFile1) {
            //execute aiFile1 if exists
            console.log(`Executing ${aiFile1}`);
            spawn('python3', [`./hackagames/game-risky/${aiFile1}`]);
        }
        else {
            //execute aiFile2 if exists
            console.log(`Executing ${aiFile2}`);
            spawn('python3', [`./hackagames/game-risky/${aiFile2}`]);
        }
        await server.connectIfNeeded();

        res.send(tabletop);
    }
}

exports.startFile = async (req, res) => {
    if(!req.files) {
        res.send({
            status: false,
            message: 'No file uploaded'
        });
    } else {
        let file = req.files.file;

        file.mv('./hackagames/game-risky/' + file.name);

        const { spawn } = require('child_process');
        const server = ServerWrapper.getServerWrapper();

        console.log(`Executing ${file.name}`);
            spawn('python3', [`./hackagames/game-risky/${file.name}`]);

        await server.connectIfNeeded();

        res.send(tabletop);


    }

    /* if(aiFile1 && aiFile2) {
        //execute the py files if they exist
        
    }
    else {
        const { spawn } = require('child_process');
        const server = ServerWrapper.getServerWrapper();
        if(aiFile1) {
            //execute aiFile1 if exists
            
            console.log(`Executing ${aiFile1}`);
            spawn('python3', [`./hackagames/game-risky/${aiFile1}`]);
        }
        else {
            //execute aiFile2 if exists
            console.log(`Executing ${aiFile2}`);
            spawn('python3', [`./hackagames/game-risky/${aiFile2}`]);
        }
        await server.connectIfNeeded();

        res.send(tabletop);
    }*/
}