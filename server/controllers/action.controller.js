const ServerWrapper = require("../utils/telnet-client");
const tabletop = require('../models/tabletop');

exports.move = async (req, res) => {
    const server = ServerWrapper.getServerWrapper();
    let move = req.body;

    await server.send(`move ${move.positionx} ${move.positiony} ${move.strength}`);
    console.log(`move ${move.positionx} ${move.positiony} ${move.strength}`);

    res.send(tabletop);
}

exports.sleep = async (req, res) => {
    const server = ServerWrapper.getServerWrapper();
    let state = 'sleep';

    await server.send(state);
    console.log("sleepy");

    res.send(tabletop);
}

exports.grow = async (req, res) => {
    const server = ServerWrapper.getServerWrapper();
    let grown = req.body.growx;

    await server.send("grow " + grown);
    console.log(`grow ${grown}`);

    res.send(tabletop);
}