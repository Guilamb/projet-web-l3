const controller = require("../controllers/action.controller");

module.exports = function(app) {
    app.use(function(req, res, next) {
      res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
      );
      next();
    });
    app.post("/api/action/move", controller.move);
    app.post("/api/action/grow", controller.grow);
    app.post("/api/action/sleep", controller.sleep);
};