const net = require('net');
const { parse } = require('./parser');
const { tabletop, resetTabletop } = require('../models/tabletop');

module.exports = class ServerWrapper{
    static serverWrapper;
    client;

    static getServerWrapper() {
        if(!ServerWrapper.serverWrapper) {
            ServerWrapper.serverWrapper = new ServerWrapper();
        }
        return ServerWrapper.serverWrapper;
    }

    async connectIfNeeded() {
        if(!this.client) {
            this.client = new net.Socket();

           
            
            this.client.on('close', function() {
                console.log('Connection closed');
                this.client = null;
                resetTabletop(tabletop)
            });

            await new Promise((resolve) => {
                this.client.on('data', (data) => {
                    if(parse(tabletop, data)) {
                        resolve();
                    }
                })
                this.client.connect(14001, '127.0.0.1', () => {
                    console.log('Connected');
                });
            });
            this.client.removeAllListeners('data');
        }
    }

    close() {
        this.client.end(() => console.log("Connection closed"));
        resetTabletop(tabletop);
    }

    async send(cmd) {
        console.log(tabletop);
        this.client.write(cmd);
        await new Promise((resolve) => {
            resetTabletop(tabletop);
            this.client.on('data', (data) => {
                if(parse(tabletop, data)) {
                    resolve();
                }
            })
        });
        this.client.removeAllListeners('data');
        console.log(tabletop)
    }

}