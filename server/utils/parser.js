// j'espère ne plus jamais être autorisé à coder en javascript
module.exports.parse = (tabletop, data) => {
        //data parsing
        //console.log(tabletop)
        const content = data.toString();
        const splitedContent = content.split("\n");
        console.log(splitedContent);
        let isOver = false;
        if (content.includes("Your-turn:") || content.includes("End:") || content.includes("Piece:") || content.includes("Game:") || content.includes("Player:")) {
            splitedContent.forEach((line) => {
                const splitedLine = line.split(" ");
                if (line.includes("Piece:")) {
                    tabletop.troupes.push({
                        cell: splitedLine[1],
                        nbPions: splitedLine[7],
                        nbCoups: splitedLine[8],
                        joueur: splitedLine[4],
                    });
                } else if (line.includes("Game:")) {
                    tabletop.toursRestants = splitedLine[1];
                } else if (line.includes("Player:")) {
                    tabletop.scoreJ1 = splitedLine[3];
                    tabletop.scoreJ2 = splitedLine[4];
                } else if (line.includes("End:")) {
                    tabletop.winner = splitedLine[1];
                    isOver = true;
                }
                else if (line.includes('Your-turn:')){
                    isOver = true;
                }
            });
        }
        return isOver;
    };