# projet_web_l3

Pour lancer le projet :
### Front
`npm install` puis `npm run serve` depuis le dossier "front"
### Back
`npm install` puis `node server.js` depuis le dossier "server"

**Il faut avoir une base de données mongoDb locale nommée webproject pour l'authentification**

**OU**

 utiliser une connexion différente de celle de l'imt et changer `const isLocal = true` (ligne 19, server/server.js) par `const isLocal = false` 
 
 *Nous recommandons quand même une bdd locale, nous avons eu beaucoup de soucis avec Atlas*

**Pour lancer une partie, il est nécessaire de lancer le jeu à part**
