import axios from 'axios';

const API_URL = 'http://localhost:8080/api/';

class GameService {
    startGame(aiFile1, aiFile2) {
        return axios.post(API_URL + 'game/start', {
            aiFile1: aiFile1,
            aiFile2: aiFile2
        });
    }

    startGameWithFile(file) {
        var formData = new FormData();
        formData.append("file", file);
        return axios.post(API_URL + 'game/startfile', formData, {
            headers: {
              'Content-Type': 'multipart/form-data'
            }
        });
    }

    move(positionx, positiony, strength) {
        return axios.post(API_URL + 'action/move', {
            positionx: positionx,
            positiony: positiony,
            strength: strength
        });
    }

    grow(growx) {
        return axios.post(API_URL + 'action/grow', {
            growx: growx
        });
    }

    sleep() {
        return axios.post(API_URL + 'action/sleep');
    }
}

export default new GameService();
