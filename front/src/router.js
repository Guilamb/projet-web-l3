import { createWebHistory, createRouter } from "vue-router";
import Home from "./components/Home.vue";
import Login from "./components/Login.vue";
import Register from "./components/Register.vue";
import Game from "./components/Game.vue";
import HistoryComponent from "./components/HistoryComponent.vue";
const Profile = () => import("./components/Profile.vue")
const BoardUser = () => import("./components/BoardUser.vue")

const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
  },
  {
    path: "/home",
    component: Home,
  },
  {
    path: "/login",
    component: Login,
  },
  {
    path: "/register",
    component: Register,
  },
  {
    path: "/profile",
    name: "profile",
    // lazy-loaded
    component: Profile,
  },
  {
    path: "/user",
    name: "user",
    // lazy-loaded
    component: BoardUser,
  },
  {
    path: "/game",
    name: 'game',
    component: Game,
  },
  {
    path: "/history",
    name: "history",
    component: HistoryComponent
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;